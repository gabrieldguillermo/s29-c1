// Part 2: Simple Server - Node.js

/*
	1. Create an index.js file inside of the activity folder. 
	2.Import the http module using the required directive. Create a variable port and assign it with the value of 8000.
	3. Create a server using the createServer method that will listen in to the port provided above.
	4. Console log in the terminal a message when the server is successfully running.
	5. Create a condition that when the login route, the register route, the homepage and the usersProfile route is accessed, it will print a message specific to the page accessed.
	6. Access each routes to test if it’s working as intended. Save a screenshot for each test.
	7. Create a condition for any other routes that will return an error message.

*/
// Code here:
const http = require('http');

const port = 8000

const server = http.createServer(function(request, response){

	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type' : 'text-plain'});
		response.end('Welcome to login page!')
	} else if(request.url == '/register'){
		response.writeHead(200, {'Content-Type' : 'text-plain'})
		response.end('Welcome to register page!')
	} else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text-plain'})
		response.end('Welcome to home page!')
	} else if(request.url == '/usersProfile'){
		response.writeHead(200, {'Content-Type' : 'text-plain'})
		response.end('Welcome to Users Profile page!')
	}
})

server.listen(port)

console.log(`Server is now accessible at localhost : ${port}`);
